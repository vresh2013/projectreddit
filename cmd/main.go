package main

import (
	"net/http"

	"projectreddit/pkg/handlers"
	"projectreddit/pkg/items"
	"projectreddit/pkg/jwtToken"
	"projectreddit/pkg/middleware"
	"projectreddit/pkg/users"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetReportCaller(true)
	logger := logrus.WithFields(logrus.Fields{})

	tokenHadler := jwtToken.TokenHadler{}

	userRepo, err := users.NewUserRepo()
	if err != nil {
		logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot create users repo")
	}
	userHandler := &handlers.UserHandler{
		Repo:         userRepo,
		Logger:       logger,
		TokenHandler: tokenHadler,
	}

	postRepo, err := items.NewPostRepo()
	if err != nil {
		logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot create posts repo")
	}
	postHandler := &handlers.PostHandler{
		Repo:         postRepo,
		Logger:       logger,
		TokenHandler: tokenHadler,
	}

	// sessDB, err := session.CreateSessDB()
	// if err != nil {
	// 	logger.WithFields(logrus.Fields{
	// 		"error": err,
	// 	}).Error("cannot create sessions DB")
	// }

	r := mux.NewRouter()

	r.HandleFunc("/", handlers.Start)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(`./template/static`))))

	r.HandleFunc("/api/posts/", postHandler.GetPosts).Methods("GET")
	r.HandleFunc("/api/posts", postHandler.NewPost).Methods("POST")

	r.HandleFunc("/api/login", userHandler.Login).Methods("POST")
	r.HandleFunc("/api/register", userHandler.Register).Methods("POST")

	r.HandleFunc("/api/user/{username}", postHandler.GetPostsByUsername).Methods("GET")

	r.HandleFunc("/api/posts/{category}", postHandler.GetCategory).Methods("GET")

	r.HandleFunc("/api/post/{post_id}", postHandler.GetPostByID).Methods("GET")
	r.HandleFunc("/api/post/{post_id}", postHandler.NewComment).Methods("POST")
	r.HandleFunc("/api/post/{post_id}", postHandler.DeletePost).Methods("DELETE")
	r.HandleFunc("/api/post/{post_id}/{comment_id}", postHandler.DeleteComment).Methods("DELETE")

	r.HandleFunc("/api/post/{post_id}/upvote", postHandler.Upvote).Methods("GET")
	r.HandleFunc("/api/post/{post_id}/downvote", postHandler.Downvote).Methods("GET")
	r.HandleFunc("/api/post/{post_id}/unvote", postHandler.Unvote).Methods("GET")

	r.PathPrefix("/").HandlerFunc(handlers.Start)

	port := ":8080"
	logrus.WithFields(logrus.Fields{
		"type": "START",
		"port": port,
	}).Info("starting server")

	mux := middleware.Auth(logger, r, tokenHadler)
	mux = middleware.AccessLog(logger, mux)
	mux = middleware.Panic(logger, mux)

	err = http.ListenAndServe(port, mux)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"error": err,
		}).Error("crudapp main: error happened")
		return
	}
}
