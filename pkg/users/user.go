package users

import (
	"database/sql"
	"errors"

	_ "github.com/go-sql-driver/mysql"
)

var (
	ErrUserExist  = errors.New("already exist")
	ErrNoSuchUser = errors.New("no such user")
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
	// Password []byte `json:"password"`
	ID string `json:"user_id"`
}

type UserRepo struct {
	Data *sql.DB
}

func NewUserRepo() (*UserRepo, error) {
	dsn := "root:good_password@tcp(localhost:3306)/users_and_sess?"
	dsn += "&charset=utf8"
	dsn += "&interpolateParams=true"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(10)
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	userRepo := &UserRepo{
		Data: db,
	}
	return userRepo, err
}

func (repo *UserRepo) Add(user User) error {
	s := ""
	err := repo.Data.QueryRow("SELECT username FROM users WHERE username = ?", user.Username).Scan(&s)
	if !errors.Is(sql.ErrNoRows, err) {
		return err
	}

	_, err = repo.Data.Exec("INSERT INTO users (`userID`, `username`, `password`) VALUES (?, ?, ?)", user.ID, user.Username, []byte(user.Password))
	if err != nil {
		return err
	}
	return nil
}

func (repo UserRepo) FindByUsername(username string) (User, error) {
	user := User{}
	row := repo.Data.QueryRow("SELECT userID, password FROM users WHERE username = ?", username)
	bytePassw := make([]byte, 0)
	err := row.Scan(&user.ID, &bytePassw)
	user.Password = string(bytePassw)
	user.Username = username
	if err != nil {
		if errors.Is(sql.ErrNoRows, err) {
			return user, ErrNoSuchUser
		}
		return user, err
	}
	return user, nil
}
