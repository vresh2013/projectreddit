package users

//go:generate mockgen -source=user.go -destination=../handlers/user_mock.go -package=handlers UserDB
type UserDB interface {
	Add(User) error
	FindByUsername(string) (User, error)
}
