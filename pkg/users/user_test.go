package users

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

// go test -coverprofile="cover.out"
// go tool cover -html="cover.out" -o cover.html

func TestAddOK(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cannot create mock: %v", err)
	}
	defer db.Close()

	user := User{
		Username: "username",
		Password: "password",
		ID:       "id",
	}

	mock.ExpectQuery("SELECT username FROM users WHERE username = ").
		WithArgs(user.Username).
		WillReturnRows(sqlmock.NewRows(nil))
	mock.ExpectExec("INSERT INTO users").
		WithArgs(user.ID, user.Username, user.Password).
		WillReturnResult(sqlmock.NewResult(0, 0))

	userRepo := UserRepo{
		Data: db,
	}
	err = userRepo.Add(user)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
}

func TestAddErr1(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cannot create mock: %v", err)
	}
	defer db.Close()

	user := User{
		Username: "username",
		Password: "password",
		ID:       "id",
	}
	someErr := fmt.Errorf("some error")

	mock.ExpectQuery("SELECT username FROM users WHERE username = ").
		WithArgs(user.Username).
		WillReturnError(someErr)

	userRepo := UserRepo{
		Data: db,
	}
	err = userRepo.Add(user)
	if !errors.Is(someErr, err) {
		t.Errorf("expected error: %v, got error: %v", someErr, err)
	}
}

func TestAddErr2(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cannot create mock: %v", err)
	}
	defer db.Close()

	user := User{
		Username: "username",
		Password: "password",
		ID:       "id",
	}
	someErr := fmt.Errorf("some error")

	mock.ExpectQuery("SELECT username FROM users WHERE username = ").
		WithArgs(user.Username).
		WillReturnRows(sqlmock.NewRows(nil))
	mock.ExpectExec("INSERT INTO users").
		WithArgs(user.ID, user.Username, user.Password).
		WillReturnError(someErr)

	userRepo := UserRepo{
		Data: db,
	}
	err = userRepo.Add(user)
	if !errors.Is(someErr, err) {
		t.Errorf("expected error: %v, got error: %v", someErr, err)
	}
}

func TestFindByUsernameOK(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cannot create mock: %v", err)
	}
	defer db.Close()

	user := User{
		Username: "username",
		ID:       "id",
		Password: "password",
	}
	rows := sqlmock.NewRows([]string{"userID", "password"})
	rows.AddRow(user.ID, user.Password)

	mock.ExpectQuery("SELECT userID, password FROM users WHERE username =").
		WithArgs(user.Username).
		WillReturnRows(rows)

	userRepo := UserRepo{
		Data: db,
	}
	ansUser, err := userRepo.FindByUsername(user.Username)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	assert.Equal(t, ansUser, user)
}

func TestFindByUsernameNoRows(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cannot create mock: %v", err)
	}
	defer db.Close()

	user := User{
		Username: "username",
		ID:       "id",
		Password: "password",
	}
	rows := sqlmock.NewRows([]string{"userID", "password"})

	mock.ExpectQuery("SELECT userID, password FROM users WHERE username =").
		WithArgs(user.Username).
		WillReturnRows(rows)

	userRepo := UserRepo{
		Data: db,
	}
	_, err = userRepo.FindByUsername(user.Username)
	assert.Equal(t, ErrNoSuchUser, err)
}

func TestFindByUsernameErr(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cannot create mock: %v", err)
	}
	defer db.Close()

	user := User{
		Username: "username",
		ID:       "id",
		Password: "password",
	}
	someErr := fmt.Errorf("some error")

	mock.ExpectQuery("SELECT userID, password FROM users WHERE username =").
		WithArgs(user.Username).
		WillReturnError(someErr)

	userRepo := UserRepo{
		Data: db,
	}
	_, err = userRepo.FindByUsername(user.Username)
	assert.Equal(t, someErr, err)
}
