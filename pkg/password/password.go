package password

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"strings"

	"golang.org/x/crypto/argon2"
)

type argonParams struct {
	memory     uint32
	time       uint32
	threads    uint8
	saltLength int
	keyLength  uint32
}

func generateSalt(len int) ([]byte, error) {
	salt := make([]byte, len)
	_, err := rand.Read(salt)
	if err != nil {
		return salt, err
	}
	return salt, nil
}

func generateHashPass(salt []byte, plainPassword string, p argonParams) string {
	// hashedPass := argon2.IDKey([]byte(plainPassword), salt, 1, 64*1024, 4, 32)
	hashedPass := argon2.IDKey([]byte(plainPassword), salt, p.time, p.memory, p.threads, p.keyLength)
	return fmt.Sprintf("$argon2id$v=%d$m=%d,t=%d,th=%d$%s$%s", argon2.Version, p.memory, p.time, p.threads, salt, hashedPass)
}

func CreateHashPassw(plainPassw string) (string, error) {
	p := argonParams{
		saltLength: 8,
		keyLength:  32,
		memory:     64 * 1024,
		time:       1,
		threads:    4,
	}
	salt, err := generateSalt(p.saltLength)
	if err != nil {
		return "", err
	}
	return generateHashPass(salt, plainPassw, p), nil
}

// vesion, params, salt, hashPassw, err
func decodeStoredPassw(storedHash string) (string, argonParams, []byte, []byte, error) {
	// "$argon2id$v=%d$m=%d,t=%d,th=%d$%s$%s"
	vals := strings.Split(storedHash, "$")
	if len(vals) != 6 {
		return "", argonParams{}, nil, nil, fmt.Errorf("not enough params: %v", vals)
	}

	version := ""
	_, err := fmt.Sscanf(vals[2], "v=%s", &version)
	if err != nil {
		return "", argonParams{}, nil, nil, fmt.Errorf("cannot read version: %s", vals[2])
	}

	p := argonParams{}
	_, err = fmt.Sscanf(vals[3], "m=%d,t=%d,th=%d", &p.memory, &p.time, &p.threads)
	if err != nil {
		return "", argonParams{}, nil, nil, fmt.Errorf("cannot read params: %s", vals[3])
	}

	salt := []byte(vals[4])
	p.saltLength = len(salt)

	hashPassw := []byte(vals[5])
	p.keyLength = uint32(len(hashPassw))

	return version, p, salt, hashPassw, nil
}

// true - совпадают
func CheckPassword(noHashPassw, storedHash string) (bool, error) {
	_, p, salt, hashPassw, err := decodeStoredPassw(storedHash)
	if err != nil {
		return false, err
	}
	newHash := argon2.IDKey([]byte(noHashPassw), salt, p.time, p.memory, p.threads, p.keyLength)
	return bytes.Equal(hashPassw, newHash), nil
}
