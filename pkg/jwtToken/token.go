package jwtToken

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type ContextKey string

const (
	KeyJwtUser = ContextKey("contex key")
)

var (
	TokenSecret = []byte("супер-бупер секретный ключ, чтоб тебя!")
	ErrNoToken  = errors.New("no token")
	letterRunes = []rune("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

type JwtUser struct {
	Username string `json:"username" valid:"-"`
	ID       string `json:"id" valid:"-"`
}

type TokenClaims struct {
	JwtUser `json:"user"`
	jwt.StandardClaims
}

type Token struct {
	Token string `json:"token"`
}

//go:generate mockgen -source=token.go -destination=../handlers/token_mock.go -package=handlers TokenHandler
type TokenHandlerI interface {
	CreateToken(username string, userID string) (string, error)
	UnmarshalToken(strToken string) (JwtUser, error)
	RandStringRunes() string
}

type TokenHadler struct{}

func (TokenHadler) CreateToken(username string, userID string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, TokenClaims{
		JwtUser{
			Username: username,
			ID:       userID,
		},
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(24 * time.Hour).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	})
	return token.SignedString(TokenSecret)
}

func (TokenHadler) UnmarshalToken(strToken string) (JwtUser, error) {
	hashSecretGetter := func(token *jwt.Token) (interface{}, error) {
		method, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok || method.Alg() != "HS256" {
			return nil, fmt.Errorf("bad sign method")
		}
		return TokenSecret, nil
	}
	token, err := jwt.Parse(strToken, hashSecretGetter)
	if err != nil || !token.Valid {
		return JwtUser{}, fmt.Errorf("cannot parse token")
	}
	tokenData, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return JwtUser{}, fmt.Errorf("cannot get token data")
	}
	m, ok := tokenData["user"]
	if !ok {
		return JwtUser{}, fmt.Errorf("cannot get user from token data")
	}
	user := m.(map[string]interface{})
	username, ok := user["username"]
	if !ok {
		return JwtUser{}, fmt.Errorf("cannot get username from token data")
	}
	id, ok := user["id"]
	if !ok {
		return JwtUser{}, fmt.Errorf("cannot get id from token data")
	}

	strUsername, ok := username.(string)
	if !ok {
		return JwtUser{}, fmt.Errorf("cannot get username from token data")
	}
	strID, ok := id.(string)
	if !ok {
		return JwtUser{}, fmt.Errorf("cannot get id from token data")
	}

	return JwtUser{
		Username: strUsername,
		ID:       strID,
	}, nil
}

func (TokenHadler) RandStringRunes() string {
	// не нужно, т.к. из слайса можно читать параллельно
	// mu := &sync.Mutex{}
	// mu.Lock()
	// defer mu.Unlock()

	b := make([]rune, 32)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
