package items

//go:generate mockgen -source=post.go -destination=../handlers/post_mock.go -package=handlers PostDB
type PostDB interface {
	AllPosts() ([]*Post, error)
	NewPost(Post) error
	GetPostByID(string) (*Post, error)
	GetCategory(string) ([]*Post, error)
	GetPostsByUsername(string) ([]*Post, error)
	DeleteComment(postID, commentID string) (Post, error)
	RepoVote(postID, userID string, vote int) (Post, error)
	NewComment(Comment, string) (Post, error)
	DeletePost(postID string) error
}
