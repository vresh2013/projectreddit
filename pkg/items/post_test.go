package items

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

// go test -coverprofile="cover.out"
// go tool cover -html="cover.out" -o cover.html

func TestUpdateScoreAndPercent(t *testing.T) {
	post := Post{
		Score: -1,
		Votes: []*Vote{
			{
				UserID: "asd",
				Vote:   1,
			},
			{
				UserID: "1asd",
				Vote:   -1,
			},
		},
	}
	post.UpdateScoreAndPercent()
	assert.Equal(t, 0, post.Score)
	assert.Equal(t, 50, post.UpvotePercentage)

	post = Post{
		Score:            -1,
		UpvotePercentage: -100,
		Votes:            make([]*Vote, 0, 1),
	}
	post.UpdateScoreAndPercent()
	assert.Equal(t, 0, post.Score)
	assert.Equal(t, 0, post.UpvotePercentage)
}

func TestFindVote(t *testing.T) {
	post := Post{
		Votes: []*Vote{
			{
				UserID: "asd",
				Vote:   1,
			},
			{
				UserID: "1asd",
				Vote:   -1,
			},
		},
	}
	ind, vote, isFound := post.findVote("asd")
	assert.Equal(t, 0, ind)
	assert.Equal(t, &Vote{
		UserID: "asd",
		Vote:   1,
	}, vote)
	assert.Equal(t, true, isFound)

	ind, vote, isFound = post.findVote("no user")
	assert.Equal(t, -1, ind)
	assert.Equal(t, &Vote{}, vote)
	assert.Equal(t, false, isFound)
}

func TestVote(t *testing.T) {
	post := Post{
		Votes: make([]*Vote, 0),
	}
	userID := "user_id"
	vote := 1
	post.Vote(userID, vote)
	expPost := Post{
		Score:            1,
		UpvotePercentage: 100,
		Votes: []*Vote{
			{
				UserID: userID,
				Vote:   1,
			},
		},
	}
	assert.Equal(t, expPost, post)

	userID1 := "other"
	vote1 := -1
	post = Post{
		Score:            1,
		UpvotePercentage: 100,
		Votes: []*Vote{
			{
				UserID: userID,
				Vote:   1,
			},
		},
	}
	post.Vote(userID1, vote1)
	expPost = Post{
		Score:            0,
		UpvotePercentage: 50,
		Votes: []*Vote{
			{
				UserID: userID,
				Vote:   1,
			},
			{
				UserID: userID1,
				Vote:   vote1,
			},
		},
	}
	assert.Equal(t, expPost, post)

	post = Post{
		Score:            0,
		UpvotePercentage: 50,
		Votes: []*Vote{
			{
				UserID: userID,
				Vote:   1,
			},
			{
				UserID: userID1,
				Vote:   vote1,
			},
		},
	}
	post.Vote(userID1, 1)
	expPost = Post{
		Score:            2,
		UpvotePercentage: 100,
		Votes: []*Vote{
			{
				UserID: userID,
				Vote:   1,
			},
			{
				UserID: userID1,
				Vote:   1,
			},
		},
	}
	assert.Equal(t, expPost, post)
}

func TestUnvote(t *testing.T) {
	userID := "userID"
	post := Post{
		Score:            1,
		UpvotePercentage: 100,
		Votes: []*Vote{
			{
				UserID: userID,
				Vote:   1,
			},
		},
	}
	err := post.Unvote(userID)
	if err != nil {
		t.Errorf("unexpected error %v", err)
	}
	expPost := Post{
		Score:            0,
		UpvotePercentage: 0,
		Votes:            make([]*Vote, 0),
	}
	assert.Equal(t, expPost, post)

	post = expPost
	err = post.Unvote(userID)
	assert.Equal(t, fmt.Errorf("no such vote"), err)

	post = Post{
		Score:            1,
		UpvotePercentage: 100,
		Votes: []*Vote{
			{
				UserID: userID,
				Vote:   1,
			},
		},
	}
	err = post.Unvote("wrong userID")
	assert.Equal(t, fmt.Errorf("no such vote"), err)
}
