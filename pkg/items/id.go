package items

import (
	// "crypto/rand"
	"math/rand"
)

var (
	letterRunes = []rune("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

func RandStringRunes() string {
	// не нужно, т.к. из слайса можно читать параллельно
	// mu := &sync.Mutex{}
	// mu.Lock()
	// defer mu.Unlock()

	b := make([]rune, 32)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
