package items

import (
	"errors"
	"fmt"
	"time"

	"github.com/asaskevich/govalidator"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrNoSuchPost = errors.New("no such post")
	ErrInvalid    = errors.New("cannot past validation")
)

type Vote struct {
	UserID string `json:"user_id"`
	Vote   int    `json:"vote" valid:"in(1|-1)"`
}

type Author struct {
	Username string `json:"username" valid:"required" bson:"username"`
	ID       string `json:"id" valid:"required" bson:"id"`
}

type Comment struct {
	Created time.Time `json:"created" valid:"required" bson:"created"`
	Author  Author    `json:"author" valid:"required" bson:"author"`
	Body    string    `json:"body" valid:"required" bson:"body"`
	ID      string    `json:"id" valid:"required" bson:"id"`
}

type Post struct {
	Score    int       `json:"score" valid:"int" bson:"score"`
	Views    int       `json:"views" valid:"int" bson:"views"`
	Type     string    `json:"type" valid:"in(text|link)" schema:"type" bson:"type"`
	Title    string    `json:"title" valid:"required" schema:"title" bson:"title"`
	URL      string    `json:"url,omitempty" valid:"url,optional" schema:"url" bson:"url"`
	Author   Author    `json:"author" valid:"required" bson:"author"`
	Category string    `json:"category" valid:"in(music|funny|videos|programming|news|fashion)" schema:"category" bson:"category"`
	Text     string    `json:"text,omitempty" valid:",optional" schema:"text" bson:"text"`
	Votes    []*Vote   `json:"votes" bson:"votes"`
	Comments []Comment `json:"comments" bson:"comments"`
	// как валидировать время?
	Created          time.Time `json:"created" valid:"-" bson:"created"`
	UpvotePercentage int       `json:"upvotePercentage" valid:"int" bson:"upvotePercentage"`
	ID               string    `json:"id" valid:"-" bson:"id"`
}

func (post *Post) UpdateScoreAndPercent() {
	if len(post.Votes) == 0 {
		post.Score = 0
		post.UpvotePercentage = 0
		return
	}
	score := 0
	for _, vote := range post.Votes {
		score += vote.Vote
	}
	post.Score = score

	if len(post.Votes) == 0 {
		post.UpvotePercentage = 0
		return
	}
	ups := 0
	downs := 0
	for _, vt := range post.Votes {
		switch vt.Vote {
		case 1:
			ups++
		case -1:
			downs++
		}
	}
	post.UpvotePercentage = ups * 100 / (ups + downs)
}

func (post Post) findVote(userID string) (int, *Vote, bool) {
	for i, vote := range post.Votes {
		if vote.UserID == userID {
			return i, vote, true
		}
	}
	return -1, &Vote{}, false
}

func (post *Post) Vote(userID string, num int) {
	if len(post.Votes) == 0 {
		post.Votes = append(post.Votes, &Vote{
			UserID: userID,
			Vote:   num,
		})
		post.UpdateScoreAndPercent()
		return
	}

	_, oldVote, isFound := post.findVote(userID)
	// не нашелся
	if !isFound {
		post.Votes = append(post.Votes, &Vote{
			UserID: userID,
			Vote:   num,
		})
		// нашелся
	} else {
		oldVote.Vote = num
	}

	post.UpdateScoreAndPercent()
}

func (post *Post) Unvote(userID string) error {
	if len(post.Votes) == 0 {
		return fmt.Errorf("no such vote")
	}

	j, _, isFound := post.findVote(userID)
	if !isFound {
		return fmt.Errorf("no such vote")
	}
	post.Votes = append(post.Votes[:j], post.Votes[(j+1):]...)

	post.UpdateScoreAndPercent()
	return nil
}

type PostRepo struct {
	Data *mgo.Collection
	Sess *mgo.Session
}

func NewPostRepo() (*PostRepo, error) {
	sess, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		return nil, err
	}
	// если коллекции не будет, то она создасться автоматически
	collection := sess.DB("coursera").C("posts")

	pr := PostRepo{
		Data: collection,
		Sess: sess,
	}
	return &pr, nil
	// pr := PostRepo{
	// 	Data: []*Post{
	// 		{
	// 			Score: 0,
	// 			Views: 4,
	// 			Type:  "link",
	// 			Title: "adele set fire to the rain",
	// 			URL:   "https://www.youtube.com/watch?v=Ri7-vnrJD3k&ab_channel=AdeleVEVO",
	// 			Author: jwt.JwtUser{
	// 				Username: "alloy",
	// 				ID:       "604d1e96a28b5000082b88f4",
	// 			},
	// 			Category: "music",
	// 			Votes:    make([]*Vote, 0, 1),
	// 			Comments: []Comment{
	// 				{
	// 					Created: time.Now(),
	// 					Author: jwt.JwtUser{
	// 						Username: "alloy",
	// 						ID:       "604d1e96a28b5000082b88f4",
	// 					},
	// 					Body: "some comment",
	// 					ID:   "604d2147e2b2e50007c9219e",
	// 				},
	// 			},
	// 			Created:          time.Now(),
	// 			UpvotePercentage: 0,
	// 			ID:               "604d20a2a28b5000082b88f5",
	// 		},
	// 	},
	// }
}

func (repo *PostRepo) AllPosts() ([]*Post, error) {
	posts := make([]*Post, 0, 1)
	// err := repo.Data.Find(bson.M{}).All(&posts)
	err := repo.Data.Find(bson.M{}).Sort("-score").All(&posts)
	if err != nil {
		return nil, err
	}

	// sort.Slice(posts, func(i, j int) bool {
	// 	return posts[i].Score < posts[j].Score
	// })
	return posts, nil
}

func (repo *PostRepo) NewPost(post Post) error {
	_, err := govalidator.ValidateStruct(&post)
	if err != nil {
		return ErrInvalid
	}
	err = repo.Data.Insert(post)
	if err != nil {
		return err
	}
	return nil
}

func (repo PostRepo) GetPostByID(id string) (*Post, error) {
	post := Post{}
	err := repo.Data.Find(bson.M{"id": id}).One(&post)
	if err != nil {
		return nil, err
	}
	return &post, nil
}

func (repo PostRepo) GetCategory(category string) ([]*Post, error) {
	posts := make([]*Post, 0, 10)
	err := repo.Data.Find(bson.M{"category": category}).All(&posts)
	if err != nil {
		return nil, err
	}
	return posts, nil
}

func (repo PostRepo) GetPostsByUsername(username string) ([]*Post, error) {
	posts := make([]*Post, 0, 10)
	err := repo.Data.Find(bson.M{"author.username": username}).All(&posts)
	if err != nil {
		return nil, err
	}
	return posts, nil
}

func (repo *PostRepo) DeleteComment(postID, commentID string) (Post, error) {
	post := Post{}
	err := repo.Data.Find(bson.M{"id": postID}).One(&post)
	if err != nil {
		return post, err
	}
	j := -1
	for i, comment := range post.Comments {
		if comment.ID == commentID {
			j = i
			break
		}
	}
	if j == -1 {
		return Post{}, fmt.Errorf("no comment with id=%s", commentID)
	}
	post.Comments = append(post.Comments[:j], post.Comments[(j+1):]...)
	err = repo.Data.Update(bson.M{"id": postID}, &post)
	if err != nil {
		return post, err
	}
	return post, nil
}

// vote = -1|0|1
func (repo *PostRepo) RepoVote(postID, userID string, vote int) (Post, error) {
	post := Post{}
	err := repo.Data.Find(bson.M{"id": postID}).One(&post)
	if err != nil {
		return post, err
	}

	if vote == 1 || vote == -1 {
		post.Vote(userID, vote)
	} else if vote == 0 {
		post.Unvote(userID)
	} else {
		return Post{}, fmt.Errorf("wrong vote value: %d", vote)
	}

	err = repo.Data.Update(bson.M{"id": postID}, &post)
	if err != nil {
		return post, err
	}
	return post, nil
}

func (repo *PostRepo) NewComment(comment Comment, postID string) (Post, error) {
	post := Post{}
	err := repo.Data.Find(bson.M{"id": postID}).One(&post)
	if err != nil {
		return post, err
	}
	post.Comments = append(post.Comments, comment)

	err = repo.Data.Update(bson.M{"id": postID}, &post)
	if err != nil {
		return post, err
	}
	return post, nil
}

func (repo *PostRepo) DeletePost(postID string) error {
	return repo.Data.Remove(bson.M{"id": postID})
}
