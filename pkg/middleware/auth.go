package middleware

import (
	"context"
	"errors"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"

	"projectreddit/pkg/jwtToken"
)

func GetUser(r *http.Request, tokenHandler jwtToken.TokenHandlerI) (jwtToken.JwtUser, error) {
	strToken := r.Header.Get("Authorization")
	if strToken == "" {
		return jwtToken.JwtUser{}, jwtToken.ErrNoToken
	}

	strToken = strings.TrimPrefix(strToken, "Bearer ")
	return tokenHandler.UnmarshalToken(strToken)
}

func Auth(logger *logrus.Entry, next http.Handler, tokenHandler jwtToken.TokenHandlerI) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, err := GetUser(r, tokenHandler)
		if err != nil {
			if errors.Is(err, jwtToken.ErrNoToken) {
				logger.Info("no token")
				next.ServeHTTP(w, r)
				return
			}
			http.Error(w, "Internal server error", 500)
			logrus.WithFields(logrus.Fields{
				"error": err,
			}).Error("cannot get jwt_user")
			return
		}
		logger.WithFields(logrus.Fields{
			"url":      r.URL,
			"username": user.Username,
			"id":       user.ID,
		}).Info("auth middleware")

		// sessID := r.Cookies()

		ctx := r.Context()
		ctx = context.WithValue(ctx, jwtToken.KeyJwtUser, user)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
