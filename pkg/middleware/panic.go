package middleware

import (
	"net/http"
	"runtime"

	"github.com/sirupsen/logrus"
)

func Panic(logger *logrus.Entry, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger.WithFields(logrus.Fields{
			"url": r.URL,
		}).Info("panicMiddleware")
		defer func() {
			if err := recover(); err != nil {
				tmp := make([]byte, 1000)
				runtime.Stack(tmp, true)
				logger.WithFields(logrus.Fields{
					"err":  err,
					"addr": string(tmp),
				}).Error("recovered")
				http.Error(w, "Internal server error", 500)
			}
		}()
		next.ServeHTTP(w, r)
	})
}
