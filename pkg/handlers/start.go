package handlers

import (
	"net/http"
)

func Start(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, `./template/index.html`)
}
