package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"projectreddit/pkg/items"
	"projectreddit/pkg/jwtToken"
	"strings"
	"testing"
	"time"

	gomock "github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestGetPosts(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockPostDB := NewMockPostDB(ctrl)

	postHandler := PostHandler{
		Repo: mockPostDB,
	}
	posts := []*items.Post{
		{
			Text:     "some text",
			Votes:    make([]*items.Vote, 0),
			Comments: make([]items.Comment, 0),
		},
	}
	mockPostDB.EXPECT().AllPosts().Return(posts, nil)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/api/posts/", nil)
	postHandler.GetPosts(w, r)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	defer r.Body.Close()

	expBytes, err := json.Marshal(posts)
	assert.NoError(t, err)
	assert.Equal(t, expBytes, body)
}

func TestGetPostsErr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockPostDB := NewMockPostDB(ctrl)
	logger := logrus.WithFields(logrus.Fields{})

	postHandler := PostHandler{
		Repo:   mockPostDB,
		Logger: logger,
	}
	mockPostDB.EXPECT().AllPosts().Return(nil, fmt.Errorf("some error"))

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/api/posts/", nil)
	postHandler.GetPosts(w, r)

	resp := w.Result()
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	defer r.Body.Close()

	assert.Equal(t, "some error\n", string(body))
}

func TestGetPost(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockToken := NewMockTokenHandlerI(ctrl)
	postHandler := PostHandler{
		TokenHandler: mockToken,
	}

	id := "123"
	mockToken.EXPECT().RandStringRunes().Return(id)

	postReq := postRequest{
		Category: "music",
		Text:     "some",
		Title:    "title",
		Type:     "link",
		URL:      "https://www.youtube.com/",
	}
	jsPost, err := json.Marshal(postReq)
	assert.NoError(t, err)
	reader := strings.NewReader(string(jsPost))
	// reader := bytes.NewReader(jsPost)
	// fmt.Println(reader)

	r := httptest.NewRequest(http.MethodGet, "/posts", reader)

	user := jwtToken.JwtUser{
		Username: "name",
		ID:       "id1",
	}
	ctx := context.WithValue(context.Background(), jwtToken.KeyJwtUser, user)
	r = r.WithContext(ctx)

	ansPost, err := postHandler.getPost(r)
	assert.NoError(t, err)

	expPost := items.Post{
		Score: 0,
		Views: 0,
		Type:  postReq.Type,
		Title: postReq.Title,
		URL:   postReq.URL,
		Author: items.Author{
			Username: user.Username,
			ID:       user.ID,
		},
		Category: postReq.Category,
		Text:     postReq.Text,
		// Votes: []*items.Vote{
		// 	{
		// 		UserID: user.ID,
		// 		Vote:   1,
		// 	},
		// },
		Votes:            make([]*items.Vote, 0, 1),
		Comments:         make([]items.Comment, 0, 10),
		Created:          time.Now(),
		UpvotePercentage: 100,
		ID:               id,
	}
	assert.Equal(t, expPost, ansPost)
}

func TestGetPostCannotConver(t *testing.T) {
	postHandler := PostHandler{}

	postReq := postRequest{
		Category: "music",
		Text:     "some",
		Title:    "title",
		Type:     "link",
		URL:      "https://www.youtube.com/",
	}
	jsPost, err := json.Marshal(postReq)
	assert.NoError(t, err)
	reader := strings.NewReader(string(jsPost))
	// reader := bytes.NewReader(jsPost)
	// fmt.Println(reader)

	r := httptest.NewRequest(http.MethodGet, "/posts", reader)

	user := items.Author{
		Username: "name",
		ID:       "id1",
	}
	ctx := context.WithValue(context.Background(), jwtToken.KeyJwtUser, user)
	r = r.WithContext(ctx)

	_, err = postHandler.getPost(r)
	assert.Equal(t, fmt.Errorf("cannot convert user from context: %v", user), err)
}

func TestGetPostNoCtx(t *testing.T) {
	postHandler := PostHandler{}
	postReq := postRequest{
		Category: "music",
		Text:     "some",
		Title:    "title",
		Type:     "link",
		URL:      "https://www.youtube.com/",
	}
	jsPost, err := json.Marshal(postReq)
	assert.NoError(t, err)
	reader := strings.NewReader(string(jsPost))
	r := httptest.NewRequest(http.MethodGet, "/posts", reader)

	_, err = postHandler.getPost(r)
	assert.Equal(t, fmt.Errorf("cannot get user from context"), err)
}

func TestGetPostCannotUnmarshal(t *testing.T) {
	postHandler := PostHandler{}
	reader := strings.NewReader("no post")
	r := httptest.NewRequest(http.MethodGet, "/posts", reader)

	_, err := postHandler.getPost(r)
	assert.Equal(t, fmt.Errorf("cannot unmarshal to post: %s", string("no post")), err)
}

func TestGetComment(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockToken := NewMockTokenHandlerI(ctrl)
	postHandler := PostHandler{
		TokenHandler: mockToken,
	}

	author := items.Author{
		Username: "name",
		ID:       "id1",
	}
	comment := items.Comment{
		Author: author,
		Body:   "some text",
		ID:     "comment id",
	}
	reader := strings.NewReader(fmt.Sprintf(`{"comment":"%s"}`, comment.Body))

	r := httptest.NewRequest(http.MethodPost, "/", reader)

	ctx := context.WithValue(context.Background(), jwtToken.KeyJwtUser, jwtToken.JwtUser{
		Username: author.Username,
		ID:       author.ID,
	})
	r = r.WithContext(ctx)

	mockToken.EXPECT().RandStringRunes().Return(comment.ID)

	ansComm, err := postHandler.getComment(r)
	// здесь мб расхождение по времени
	comment.Created = time.Now()

	assert.NoError(t, err)
	assert.Equal(t, comment, ansComm)
}

func TestGetCommentNoUser(t *testing.T) {
	postHandler := PostHandler{}

	author := items.Author{
		Username: "name",
		ID:       "id1",
	}
	comment := items.Comment{
		Author: author,
		Body:   "some text",
		ID:     "comment id",
	}
	reader := strings.NewReader(fmt.Sprintf(`{"comment":"%s"}`, comment.Body))

	r := httptest.NewRequest(http.MethodPost, "/", reader)

	ctx := context.WithValue(context.Background(), jwtToken.KeyJwtUser, "no author")
	r = r.WithContext(ctx)

	_, err := postHandler.getComment(r)
	assert.Equal(t, fmt.Errorf("cannot convert user from context"), err)
}

func TestGetCommentNoContext(t *testing.T) {
	postHandler := PostHandler{}

	author := items.Author{
		Username: "name",
		ID:       "id1",
	}
	comment := items.Comment{
		Author: author,
		Body:   "some text",
		ID:     "comment id",
	}
	reader := strings.NewReader(fmt.Sprintf(`{"comment":"%s"}`, comment.Body))

	r := httptest.NewRequest(http.MethodPost, "/", reader)

	_, err := postHandler.getComment(r)
	assert.Equal(t, fmt.Errorf("cannot get user from context"), err)
}

func TestGetCommentNoComment(t *testing.T) {
	postHandler := PostHandler{}

	reader := strings.NewReader(`no comment`)

	r := httptest.NewRequest(http.MethodPost, "/", reader)

	_, err := postHandler.getComment(r)
	assert.Equal(t, fmt.Errorf("cannot unmarshal comment: %s", `no comment`), err)
}

func TestNewPost(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockToken := NewMockTokenHandlerI(ctrl)
	mockPostDB := NewMockPostDB(ctrl)
	postHandler := PostHandler{
		TokenHandler: mockToken,
		Repo:         mockPostDB,
	}

	id := "123"
	mockToken.EXPECT().RandStringRunes().Return(id)

	postReq := postRequest{
		Category: "music",
		Text:     "some",
		Title:    "title",
		Type:     "link",
		URL:      "https://www.youtube.com/",
	}
	jsPost, err := json.Marshal(postReq)
	assert.NoError(t, err)
	reader := strings.NewReader(string(jsPost))
	// reader := bytes.NewReader(jsPost)
	// fmt.Println(reader)

	r := httptest.NewRequest(http.MethodGet, "/posts", reader)

	user := jwtToken.JwtUser{
		Username: "name",
		ID:       "id1",
	}
	ctx := context.WithValue(context.Background(), jwtToken.KeyJwtUser, user)
	r = r.WithContext(ctx)

	post := items.Post{
		Score: 0,
		Views: 0,
		Type:  postReq.Type,
		Title: postReq.Title,
		URL:   postReq.URL,
		Author: items.Author{
			Username: user.Username,
			ID:       user.ID,
		},
		Category:         postReq.Category,
		Text:             postReq.Text,
		Votes:            make([]*items.Vote, 0, 1),
		Comments:         make([]items.Comment, 0, 10),
		Created:          time.Now(),
		UpvotePercentage: 100,
		ID:               id,
	}

	mockPostDB.EXPECT().NewPost(post).Return(nil)
	w := httptest.NewRecorder()

	// расхождение по времени
	post.Created = time.Now()
	postHandler.NewPost(w, r)

	resp := w.Result()
	body, err := io.ReadAll(resp.Body)
	defer resp.Body.Close()
	assert.NoError(t, err)
	ansPost := items.Post{}
	err = json.Unmarshal(body, &ansPost)
	assert.NoError(t, err)

	// не проверяем время создания: оно нам не интересно
	ansPost.Created = post.Created
	assert.Equal(t, post, ansPost)
}
