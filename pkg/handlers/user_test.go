package handlers

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"projectreddit/pkg/users"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

// go test -coverprofile="cover.out"
// go tool cover -html="cover.out" -o cover.html

func TestGetUsernameAndPassword(t *testing.T) {
	username := "username"
	password := "123"
	r := httptest.NewRequest("POST", "/api/login", strings.NewReader(fmt.Sprintf(`{"username":"%s","password":"%s"}`, username, password)))
	password = fmt.Sprintf("%x", sha256.Sum256([]byte(password)))
	ansUsername, ansPassw, err := getUsernameAndPassword(r)
	assert.NoError(t, err)
	assert.Equal(t, username, ansUsername)
	assert.Equal(t, password, ansPassw)
}

func TestGetUsernameAndPasswordEOF(t *testing.T) {
	r := httptest.NewRequest("POST", "/api/login", nil)
	_, _, err := getUsernameAndPassword(r)
	assert.Equal(t, io.EOF, err)
}

func TestGetUsernameAndPasswordNoUsername(t *testing.T) {
	password := "123"
	r := httptest.NewRequest("POST", "/api/login", strings.NewReader(fmt.Sprintf(`{"username":"","password":"%s"}`, password)))
	_, _, err := getUsernameAndPassword(r)
	assert.Equal(t, fmt.Errorf("empty username"), err)
}

func TestGetUsernameAndPasswordNoPassw(t *testing.T) {
	username := "username"
	r := httptest.NewRequest("POST", "/api/login", strings.NewReader(fmt.Sprintf(`{"username":"%s","password":""}`, username)))
	_, _, err := getUsernameAndPassword(r)
	assert.Equal(t, fmt.Errorf("empty password"), err)
}

func TestSendToken(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	tokenHandlMock := NewMockTokenHandlerI(ctrl)
	username, userID := "usrname", "userID"
	token := "token"
	tokenHandlMock.EXPECT().CreateToken(username, userID).Return(token, nil)
	w := httptest.NewRecorder()

	userHandler := UserHandler{
		TokenHandler: tokenHandlMock,
	}
	userHandler.sendToken(w, username, userID)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	defer resp.Body.Close()
	got := string(body)

	expAns := fmt.Sprintf(`{"token":"%s"}`, token)
	assert.Equal(t, expAns, got)
}

func TestSendTokenErr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	tokenHandlMock := NewMockTokenHandlerI(ctrl)
	username, userID := "usrname", "userID"
	tokenHandlMock.EXPECT().CreateToken(username, userID).Return("", fmt.Errorf("some err"))

	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		TokenHandler: tokenHandlMock,
		Logger:       logger,
	}
	w := httptest.NewRecorder()
	userHandler.sendToken(w, username, userID)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	defer resp.Body.Close()
	got := string(body)

	expAns := "Internal error\n"
	assert.Equal(t, expAns, got)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func TestRegister(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	mockTokenHandler := NewMockTokenHandlerI(ctrl)
	userHandler := UserHandler{
		Repo:         mockUserDB,
		TokenHandler: mockTokenHandler,
	}

	user := users.User{
		Username: "user1",
		Password: "123",
		ID:       "user1ID",
	}
	hashPassw := fmt.Sprintf("%x", sha256.Sum256([]byte(user.Password)))
	token := "token"

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(users.User{}, users.ErrNoSuchUser)
	mockTokenHandler.EXPECT().RandStringRunes().Return(user.ID)

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, user.Password)
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))
	user.Password = hashPassw

	mockUserDB.EXPECT().Add(user).Return(nil)
	mockTokenHandler.EXPECT().CreateToken(user.Username, user.ID).Return(token, nil)

	userHandler.Register(w, r)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, fmt.Sprintf(`{"token":"%s"}`, token), string(body))
}

func TestRegisterNoReqBody(t *testing.T) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, "/api/register", nil)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Logger: logger,
	}

	userHandler.Register(w, r)
	resp := w.Result()
	assert.Equal(t, 400, resp.StatusCode)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	defer resp.Body.Close()
	assert.Equal(t, io.EOF.Error()+"\n", string(body))
}

func TestRegisterExist(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Repo:   mockUserDB,
		Logger: logger,
	}
	user := users.User{
		Username: "name",
		Password: "123",
		ID:       "id",
	}

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(user, nil)
	errs := ResponseErrorAns{
		Errors: []ResponseError{
			{
				Location: "body",
				Param:    "username",
				Value:    user.Username,
				Msg:      "already exists",
			},
		},
	}

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, user.Password)
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))

	userHandler.Register(w, r)

	resp := w.Result()
	assert.Equal(t, 422, resp.StatusCode)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	defer resp.Body.Close()

	respErrs := ResponseErrorAns{}
	err = json.Unmarshal(body, &respErrs)
	assert.NoError(t, err)
	assert.Equal(t, errs, respErrs)
}

func TestRegisterErrFindUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Repo:   mockUserDB,
		Logger: logger,
	}
	user := users.User{
		Username: "name",
		Password: "123",
		ID:       "id",
	}

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(users.User{}, io.EOF)

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, user.Password)
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))

	userHandler.Register(w, r)

	resp := w.Result()
	assert.Equal(t, 500, resp.StatusCode)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	defer resp.Body.Close()

	assert.Equal(t, "internal server error\n", string(body))
}

func TestRegisterCannotAdd(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	mockToken := NewMockTokenHandlerI(ctrl)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Repo:         mockUserDB,
		Logger:       logger,
		TokenHandler: mockToken,
	}

	user := users.User{
		Username: "user1",
		Password: "123",
		ID:       "user1ID",
	}
	hashPassw := fmt.Sprintf("%x", sha256.Sum256([]byte(user.Password)))

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(users.User{}, users.ErrNoSuchUser)
	mockToken.EXPECT().RandStringRunes().Return(user.ID)

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, user.Password)
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))
	user.Password = hashPassw

	someErr := fmt.Errorf("some err")
	mockUserDB.EXPECT().Add(user).Return(someErr)

	userHandler.Register(w, r)

	resp := w.Result()
	assert.Equal(t, 500, resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "Internal error\n", string(body))
}

func TestLogin(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	mockToken := NewMockTokenHandlerI(ctrl)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Repo:         mockUserDB,
		Logger:       logger,
		TokenHandler: mockToken,
	}

	user := users.User{
		Username: "user1",
		Password: "123",
		ID:       "user1ID",
	}

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, user.Password)
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))

	user.Password = fmt.Sprintf("%x", sha256.Sum256([]byte(user.Password)))

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(user, nil)
	token := "token"
	mockToken.EXPECT().CreateToken(user.Username, user.ID).Return(token, nil)

	userHandler.Login(w, r)

	resp := w.Result()
	assert.Equal(t, 200, resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	expAns := fmt.Sprintf(`{"token":"%s"}`, token)
	assert.Equal(t, expAns, string(body))
}

func TestLoginWrongPassw(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	mockToken := NewMockTokenHandlerI(ctrl)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Repo:         mockUserDB,
		Logger:       logger,
		TokenHandler: mockToken,
	}

	user := users.User{
		Username: "user1",
		Password: "123",
		ID:       "user1ID",
	}

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, "wrong passw")
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))

	user.Password = fmt.Sprintf("%x", sha256.Sum256([]byte(user.Password)))

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(user, nil)

	userHandler.Login(w, r)

	resp := w.Result()
	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, `{"message":"invalid password"}`+"\n", string(body))
}

func TestLoginNoUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	mockToken := NewMockTokenHandlerI(ctrl)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Repo:         mockUserDB,
		Logger:       logger,
		TokenHandler: mockToken,
	}

	user := users.User{
		Username: "user1",
		Password: "123",
		ID:       "user1ID",
	}

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, user.Password)
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))

	user.Password = fmt.Sprintf("%x", sha256.Sum256([]byte(user.Password)))

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(users.User{}, users.ErrNoSuchUser)

	userHandler.Login(w, r)

	resp := w.Result()
	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, `{"message":"user not found"}`+"\n", string(body))
}

func TestLoginErr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserDB := NewMockUserDB(ctrl)
	mockToken := NewMockTokenHandlerI(ctrl)
	logger := logrus.WithFields(logrus.Fields{})
	userHandler := UserHandler{
		Repo:         mockUserDB,
		Logger:       logger,
		TokenHandler: mockToken,
	}

	user := users.User{
		Username: "user1",
		Password: "123",
		ID:       "user1ID",
	}

	w := httptest.NewRecorder()
	s := fmt.Sprintf(`{"username":"%s","password":"%s"}`, user.Username, user.Password)
	r := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(s))

	user.Password = fmt.Sprintf("%x", sha256.Sum256([]byte(user.Password)))

	mockUserDB.EXPECT().FindByUsername(user.Username).Return(users.User{}, fmt.Errorf("some error"))

	userHandler.Login(w, r)

	resp := w.Result()
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "internal server error"+"\n", string(body))
}
