package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"projectreddit/pkg/jwtToken"
	psw "projectreddit/pkg/password"
	"projectreddit/pkg/session"
	"projectreddit/pkg/users"

	"github.com/sirupsen/logrus"
)

// const (
// 	cookieName = "session_id"
// )

type UserHandler struct {
	Repo         users.UserDB
	Logger       *logrus.Entry
	Sessions     session.SessDB
	TokenHandler jwtToken.TokenHandlerI
}

func giveHashPassw(inputPassw string) (string, error) {
	hash, err := psw.CreateHashPassw(inputPassw)
	if err != nil {
		return "", err
	}
	return hash, nil
}

// username, password_hash
func getUsernameAndPassword(r *http.Request) (string, string, error) {
	decoder := json.NewDecoder(r.Body)
	user := users.User{}
	err := decoder.Decode(&user)

	if err != nil {
		return "", "", err
	}
	if user.Password == "" {
		return "", "", fmt.Errorf("empty password")
	}
	if user.Username == "" {
		return "", "", fmt.Errorf("empty username")
	}

	return user.Username, user.Password, nil
}

func (h UserHandler) sendToken(w http.ResponseWriter, username, userID string) {
	tokenStr, err := h.TokenHandler.CreateToken(username, userID)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get token string")
		http.Error(w, "Internal error", 500)
		return
	}

	token := jwtToken.Token{
		Token: tokenStr,
	}

	h.writeJSON(w, token)
}

func (h *UserHandler) Register(w http.ResponseWriter, r *http.Request) {
	username, password, err := getUsernameAndPassword(r)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get user from POST data")
		http.Error(w, err.Error(), 400)
		return
	}

	user := users.User{
		Username: username,
		// password later
	}
	_, err = h.Repo.FindByUsername(user.Username)
	if err == nil {
		// если пользователь уже существует
		// надо отправить ответ:
		//{"errors":[{"location":"body","param":"username","value":"reverse","msg":"already exists"}]}
		errs := ResponseErrorAns{
			Errors: []ResponseError{
				{
					Location: "body",
					Param:    "username",
					Value:    user.Username,
					Msg:      "already exists",
				},
			},
		}
		err1 := ResponseErrors(w, errs)
		if err1 != nil {
			h.Logger.WithFields(logrus.Fields{
				"error": err,
			}).Error("cannot log error to client")
			http.Error(w, "internal server error", 500)
			return
		}
		return
	}
	if !errors.Is(err, users.ErrNoSuchUser) {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get user from repo")
		http.Error(w, "internal server error", 500)
		return
	}

	// уникальный ID
	// user.ID = items.RandStringRunes()
	user.ID = h.TokenHandler.RandStringRunes()
	hashedPassw, err := giveHashPassw(password)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot give hash password")
		http.Error(w, "internal server error", 500)
		return
	}
	user.Password = hashedPassw

	err = h.Repo.Add(user)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot add new user")
		http.Error(w, "Internal error", 500)
		return
	}

	h.sendToken(w, user.Username, user.ID)
}

func (h *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	username, password, err := getUsernameAndPassword(r)
	// fmt.Println("get:", password)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get user from POST data")
		http.Error(w, "Internal error", 500)
		return
	}
	inputUser := users.User{
		Username: username,
		// password later
	}

	repoUser, err := h.Repo.FindByUsername(inputUser.Username)
	if err != nil {
		if !errors.Is(err, users.ErrNoSuchUser) {
			h.Logger.WithFields(logrus.Fields{
				"error": err,
			}).Error("cannot get user from repo")
			http.Error(w, "internal server error", 500)
			return
		}
		http.Error(w, `{"message":"user not found"}`, http.StatusUnauthorized)
		h.Logger.WithFields(logrus.Fields{
			"username": inputUser.Username,
		}).Info("user not found")
		return
	}

	isEqual, err := psw.CheckPassword(password, repoUser.Password)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"err": err,
		}).Error("cannot check password")
		http.Error(w, "Internal error", 500)
		return
	}

	if !isEqual {
		http.Error(w, `{"message":"invalid password"}`, http.StatusUnauthorized)
		h.Logger.Info("invalid password")
		return
	}

	h.sendToken(w, repoUser.Username, repoUser.ID)
}

func (h UserHandler) writeJSON(w http.ResponseWriter, obj interface{}) {
	ans, err := json.Marshal(obj)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error(fmt.Sprintf("cannot marshal: %#+v", obj))
		http.Error(w, err.Error(), 500)
		return
	}
	_, err = w.Write(ans)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot write response")
		http.Error(w, err.Error(), 500)
		return
	}
}

// если при регистрации пользователь уже существует:
//{"errors":[{"location":"body","param":"username","value":"reverse","msg":"already exists"}]}
type ResponseError struct {
	Location string `json:"location"`
	Param    string `json:"param"`
	Value    string `json:"value"`
	Msg      string `json:"msg"`
}

type ResponseErrorAns struct {
	Errors []ResponseError `json:"errors"`
}

func ResponseErrors(w http.ResponseWriter, errs ResponseErrorAns) error {
	jstring, err := json.Marshal(errs)
	if err != nil {
		return err
	}
	w.WriteHeader(422)
	w.Write(jstring)

	return nil
}
