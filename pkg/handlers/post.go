package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"projectreddit/pkg/items"
	"projectreddit/pkg/jwtToken"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type postRequest struct {
	Category string `json:"category"`
	Text     string `json:"text"`
	Title    string `json:"title"`
	Type     string `json:"type"`
	URL      string `json:"url"`
}

type newComment struct {
	Comment string `json:"comment"`
}

type PostHandler struct {
	Repo         items.PostDB
	Logger       *logrus.Entry
	TokenHandler jwtToken.TokenHandlerI
	// Sess   session.SessDB
}

func (h PostHandler) GetPosts(w http.ResponseWriter, r *http.Request) {
	posts, err := h.Repo.AllPosts()
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get all posts")
		http.Error(w, err.Error(), 500)
		return
	}
	h.writeJSON(w, posts)
}

// func (h PostHandler) handleSession(r *http.Request) items.Author {

// }

func (h PostHandler) getPost(r *http.Request) (items.Post, error) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return items.Post{}, err
	}
	defer r.Body.Close()

	reqst := postRequest{}
	err = json.Unmarshal(data, &reqst)
	if err != nil {
		return items.Post{}, fmt.Errorf("cannot unmarshal to post: %s", string(data))
	}

	val := r.Context().Value(jwtToken.KeyJwtUser)
	if val == nil {
		return items.Post{}, fmt.Errorf("cannot get user from context")
	}
	user, ok := val.(jwtToken.JwtUser)
	if !ok {
		return items.Post{}, fmt.Errorf("cannot convert user from context: %v", val)
	}
	author := items.Author{
		Username: user.Username,
		ID:       user.ID,
	}

	// id := items.RandStringRunes()
	id := h.TokenHandler.RandStringRunes()
	post := items.Post{
		Score:    0,
		Views:    0,
		Type:     reqst.Type,
		Title:    reqst.Title,
		URL:      reqst.URL,
		Author:   author,
		Category: reqst.Category,
		Text:     reqst.Text,
		// Votes: []*items.Vote{
		// 	{
		// 		UserID: user.ID,
		// 		Vote:   1,
		// 	},
		// },
		Votes:            make([]*items.Vote, 0, 1),
		Comments:         make([]items.Comment, 0, 10),
		Created:          time.Now(),
		UpvotePercentage: 100,
		ID:               id,
	}

	return post, nil
}

func (h PostHandler) getComment(r *http.Request) (items.Comment, error) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return items.Comment{}, err
	}
	// надо ли закрывать Body? Это же ответ, а запрос
	defer r.Body.Close()

	newComment := newComment{}
	err = json.Unmarshal(data, &newComment)
	if err != nil {
		return items.Comment{}, fmt.Errorf("cannot unmarshal comment: %s", string(data))
	}

	// user, err := jwt.GetUser(r)
	// if err != nil {
	// 	return items.Comment{}, err
	// }
	val := r.Context().Value(jwtToken.KeyJwtUser)
	if val == nil {
		return items.Comment{}, fmt.Errorf("cannot get user from context")
	}
	user, ok := val.(jwtToken.JwtUser)
	if !ok {
		return items.Comment{}, fmt.Errorf("cannot convert user from context")
	}
	author := items.Author{
		Username: user.Username,
		ID:       user.ID,
	}

	// id := items.RandStringRunes()
	id := h.TokenHandler.RandStringRunes()
	comment := items.Comment{
		Created: time.Now(),
		Author:  author,
		Body:    newComment.Comment,
		ID:      id,
	}

	return comment, nil
}

func (h *PostHandler) NewPost(w http.ResponseWriter, r *http.Request) {
	post, err := h.getPost(r)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get post")
		http.Error(w, err.Error(), 500)
		return
	}
	err = h.Repo.NewPost(post)
	if err != nil {
		if errors.Is(items.ErrInvalid, err) {
			h.Logger.WithFields(logrus.Fields{
				"post": post,
			}).Error("wrong post arguments")
			http.Error(w, fmt.Errorf("wrong post arguments: %w", err).Error(), 400)
			return
		}
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot add new post")
		http.Error(w, err.Error(), 500)
		return
	}
	h.writeJSON(w, post)
}

func (h PostHandler) GetPostByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, ok := vars["post_id"]
	if !ok {
		// h.Logger.Errorw("cannot get post_id from router")
		h.Logger.Error("cannot get post_id from router")
		http.Error(w, "Internal server error", 400)
		return
	}
	if len(postID) == 0 {
		// h.Logger.Errorw("post_id is empty")
		h.Logger.Error("post_id is empty")
		http.Error(w, "Internal server error", 400)
		return
	}

	post, err := h.Repo.GetPostByID(postID)
	if err != nil {
		if !errors.Is(err, items.ErrNoSuchPost) {
			h.Logger.WithFields(logrus.Fields{
				"error": err,
			}).Error("repo GetPostByID: error happened")
			http.Error(w, err.Error(), 500)
			return
		}
		// h.Logger.Error("cannot get post by id")
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get post by id")
		http.Error(w, err.Error(), 400)
		return
	}
	h.writeJSON(w, post)
}

func (h *PostHandler) NewComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, ok := vars["post_id"]
	if !ok {
		h.Logger.Error("cannot get post id from router")
		http.Error(w, "Internal server error", 400)
		return
	}
	comment, err := h.getComment(r)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get comment from request")
		http.Error(w, "Internal server error", 400)
		return
	}

	post, err := h.Repo.NewComment(comment, postID)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot add new comment")
		http.Error(w, err.Error(), 500)
		return
	}

	h.writeJSON(w, post)
}

func (h *PostHandler) DeletePost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, ok := vars["post_id"]
	if !ok {
		h.Logger.Error("cannot get post id from router")
		http.Error(w, "Internal server error", 400)
		return
	}
	err := h.Repo.DeletePost(postID)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error(fmt.Sprintf("cannot delete post with id = %s", postID))
		http.Error(w, err.Error(), 500)
		return
	}
	w.Write([]byte(`{"message":"success"}`))
}

func (h *PostHandler) GetCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	category, ok := vars["category"]
	if !ok {
		h.Logger.WithFields(logrus.Fields{
			"error": fmt.Errorf("category does not exist"),
		}).Error("cannot get category from router")
		http.Error(w, "Internal server error", 500)
		return
	}

	posts, err := h.Repo.GetCategory(category)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot get category from DB")
		http.Error(w, "Internal server error", 500)
		return
	}
	h.writeJSON(w, posts)
}

func (h PostHandler) GetPostsByUsername(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	username, ok := vars["username"]
	if !ok {
		h.Logger.Error("cannot get username from router")
		http.Error(w, "cannot get username from router", 400)
		return
	}
	if username == "" {
		h.Logger.Error("username is empty")
		http.Error(w, "username is empty", 400)
		return
	}

	posts, err := h.Repo.GetPostsByUsername(username)
	if err != nil {
		h.Logger.
			WithFields(logrus.Fields{
				"error": err,
			}).Error("cannot get posts from DB")
		http.Error(w, "cannot get posts from DB", 500)
		return
	}

	h.writeJSON(w, posts)
}

func (h *PostHandler) DeleteComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, ok := vars["post_id"]
	if !ok {
		h.Logger.WithFields(logrus.Fields{
			"error": fmt.Errorf("post_id does not exist"),
		}).Error("cannot get post_id from router")
		http.Error(w, "Internal server error", 500)
		return
	}
	commentID, ok := vars["comment_id"]
	if !ok {
		h.Logger.WithFields(logrus.Fields{
			"error": fmt.Errorf("comment_id does not exist"),
		}).Error("cannot get comment_id from router")
		http.Error(w, "Internal server error", 500)
		return
	}
	if postID == "" || commentID == "" {
		h.Logger.Error("еmpty postID or commentID")
		http.Error(w, "еmpty postID or commentID", 400)
		return
	}

	post, err := h.Repo.DeleteComment(postID, commentID)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot delete comment")
		http.Error(w, "Internal server error", 500)
		return
	}

	h.writeJSON(w, post)
}

func (h *PostHandler) Upvote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, ok := vars["post_id"]
	if !ok {
		h.Logger.Error("cannot get post_id from router")
		http.Error(w, "cannot get post_id from router", 400)
		return
	}
	val := r.Context().Value(jwtToken.KeyJwtUser)
	if val == nil {
		h.Logger.Error("cannot get user from token")
		http.Error(w, "Internal server error", 500)
		return
	}
	user := val.(jwtToken.JwtUser)

	if postID == "" || user.ID == "" {
		h.Logger.Error("empty post_id or user_id")
		http.Error(w, "empty post_id or user_id", 400)
		return
	}

	post, err := h.Repo.RepoVote(postID, user.ID, 1)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot upvote")
		http.Error(w, "Internal server error", 500)
		return
	}

	h.writeJSON(w, post)
}

func (h *PostHandler) Downvote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, ok := vars["post_id"]
	if !ok {
		h.Logger.WithFields(logrus.Fields{
			"error": fmt.Errorf("post_id does not exist"),
		}).Error("cannot get post_id from router")
		http.Error(w, "Internal server error", 500)
		return
	}
	val := r.Context().Value(jwtToken.KeyJwtUser)
	if val == nil {
		h.Logger.Error("cannot get user from token")
		http.Error(w, "Internal server error", 500)
		return
	}
	user := val.(jwtToken.JwtUser)

	if postID == "" {
		h.Logger.Error("empty post_id")
		http.Error(w, "empty post_id", 400)
		return
	}
	if user.ID == "" {
		h.Logger.Error("empty user_id")
		http.Error(w, "empty user_id", 400)
		return
	}

	// только здесь отличается от Upvote
	post, err := h.Repo.RepoVote(postID, user.ID, -1)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot downvote")
		http.Error(w, "Internal server error", 500)
		return
	}

	h.writeJSON(w, post)
}

func (h *PostHandler) Unvote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postID, ok := vars["post_id"]
	if !ok {
		h.Logger.WithFields(logrus.Fields{
			"error": fmt.Errorf("post_id does not exist"),
		}).Error("cannot get post_id from router")
		http.Error(w, "Internal server error", 500)
		return
	}
	/////////////////
	val := r.Context().Value(jwtToken.KeyJwtUser)
	if val == nil {
		h.Logger.Error("cannot get user from token")
		http.Error(w, "Internal server error", 500)
		return
	}
	user := val.(jwtToken.JwtUser)

	if postID == "" || user.ID == "" {
		h.Logger.Error("empty post_id or user_id")
		http.Error(w, "empty post_id or user_id", 400)
		return
	}

	post, err := h.Repo.RepoVote(postID, user.ID, 0)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot unvote")
		http.Error(w, "Internal server error", 500)
		return
	}

	h.writeJSON(w, post)
}

func (h PostHandler) writeJSON(w http.ResponseWriter, obj interface{}) {
	ans, err := json.Marshal(obj)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error(fmt.Sprintf("cannot marshal: %#+v", obj))
		http.Error(w, err.Error(), 500)
		return
	}
	_, err = w.Write(ans)
	if err != nil {
		h.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Error("cannot write response")
		http.Error(w, err.Error(), 500)
		return
	}
}
