package session

type SessDB interface {
	CreateSessDB() (*SessRepo, error)
	CreateSess(username string) (sessID string, err error)
	GetUsername(sessID string) (string, error)
}
