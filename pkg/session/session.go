package session

import (
	"database/sql"
	"errors"
	"projectreddit/pkg/items"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type SessRepo struct {
	DB *sql.DB
}

func CreateSessDB() (*SessRepo, error) {
	dsn := "root:good_password@tcp(localhost:3306)/users_and_sess?"
	dsn += "&charset=utf8"
	dsn += "&interpolateParams=true"
	dsn += "&parseTime=true"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(10)
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	sessDB := &SessRepo{
		DB: db,
	}
	return sessDB, err
}

func (sess *SessRepo) CreateSess(username string) (sessID string, err error) {
	var expTime time.Time
	sqlID := ""
	err = sess.DB.QueryRow("SELECT id, expTime FROM SessRepo WHERE username = ?", username).Scan(&sqlID, &expTime)
	if !errors.Is(sql.ErrNoRows, err) {
		return "", err
	}
	if expTime.Before(time.Now()) {
		_, err = sess.DB.Exec("DELETE FROM SessRepo WHERE id = ?", sqlID)
		if err != nil {
			return "", err
		}
	}

	sessID = items.RandStringRunes()
	_, err = sess.DB.Exec("INSERT INTO SessRepo (`sessID`, `username`, `expTime`) VALUES (?, ?)", sessID, username, time.Now().Add(time.Hour*24))
	if err != nil {
		return "", err
	}
	return sessID, nil
}

func (sess *SessRepo) DeleteSess(sessID string) error {
	_, err := sess.DB.Exec("DELETE FROM SessRepo WHERE sessID = ?", sessID)
	if err != nil {
		return err
	}
	return nil
}

func (sess SessRepo) GetUsername(sessID string) (string, error) {
	username := ""
	err := sess.DB.QueryRow("SELECT username FROM SessRepo WHERE sessID = ?", sessID).Scan(&username)
	if err != nil {
		return "", nil
	}
	return username, nil
}
